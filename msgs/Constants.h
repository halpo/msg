//
//  Constants.h
//  msgs
//
//  Created by Paul on 24/06/2014.
//  Copyright (c) 2014 Paul. All rights reserved.
//

#define kKeyboardHeight 216
#define iOS6 [[[UIDevice currentDevice] systemVersion] floatValue] < 7.0
#define iOS7 [[[UIDevice currentDevice] systemVersion] floatValue] >= 7.0
#define iOS8 [[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0
