//
//  ViewController.m
//  msgs
//
//  Created by Paul on 24/06/2014.
//  Copyright (c) 2014 Paul. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
	[super viewDidLoad];
	[self.loginButton addTarget:self action:@selector(login) forControlEvents:UIControlEventTouchUpInside];
	[self.registerButton addTarget:self action:@selector(registration) forControlEvents:UIControlEventTouchUpInside];
	[self.rememberSwitch addTarget:self action:@selector(remChanged:) forControlEvents:UIControlEventValueChanged];
	[self.randomNameButton addTarget:self action:@selector(randomName) forControlEvents:UIControlEventTouchUpInside];
}

- (void)viewDidAppear:(BOOL)animated {
    [self showHud:YES];
	if (![[NSUserDefaults standardUserDefaults] boolForKey:@"gotNames"]) {
		//get names
		NSString *path = [[NSBundle mainBundle] pathForResource:@"firstnames" ofType:@"txt"];
		NSString *firstNames = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil];
		NSArray *firstNamesArray = [firstNames componentsSeparatedByString:@"|"];
		NSString *path2 = [[NSBundle mainBundle] pathForResource:@"surnames" ofType:@"txt"];
		NSString *surnames = [NSString stringWithContentsOfFile:path2 encoding:NSUTF8StringEncoding error:nil];
		NSArray *surnameArray = [surnames componentsSeparatedByString:@"|"];
		[[NSUserDefaults standardUserDefaults] setObject:firstNamesArray forKey:@"firstNames"];
		[[NSUserDefaults standardUserDefaults] setObject:surnameArray forKey:@"surnames"];
		[[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"gotNames"];
		[[NSUserDefaults standardUserDefaults] synchronize];
	}

	if ([PFUser currentUser]) {
		[self performSegueWithIdentifier:@"LoginSegue" sender:nil];
		return;
	}

	self.errorLabel.text = @"";
	self.userField.text = @"";
	self.passField.text = @"";

	if ([[NSUserDefaults standardUserDefaults] boolForKey:@"remember"]) {
		self.userField.text = [SSKeychain passwordForService:@"user" account:@"login"];
		self.passField.text = [SSKeychain passwordForService:@"pass" account:@"login"];
		[self.rememberSwitch setOn:YES];
	}
	else {
		[self.rememberSwitch setOn:NO];
	}
    [self showHud:NO];
}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
}

- (IBAction)bgTouch {
	[self.view endEditing:YES];
}

- (void)randomName {
	NSArray *firstNames = (NSArray *)[[NSUserDefaults standardUserDefaults] valueForKey:@"firstNames"];
	NSUInteger r1 = arc4random() % [firstNames count];
	NSArray *surnames = (NSArray *)[[NSUserDefaults standardUserDefaults] valueForKey:@"surnames"];
	NSUInteger r2 = arc4random() % [surnames count];
	NSString *name = [NSString stringWithFormat:@"%@ %@", firstNames[r1], surnames[r2]];
	self.userField.text = [name capitalizedString];
}

- (void)login {
	if (self.userField.text.length && self.passField.text.length) {
		[self showHud:YES];
		[PFUser logInWithUsernameInBackground:self.userField.text.lowercaseString password:[self hashed:self.passField.text.lowercaseString] block: ^(PFUser *user, NSError *error) {
		    if (!error) {
		        if ([[NSUserDefaults standardUserDefaults] boolForKey:@"remember"]) {
		            [SSKeychain setPassword:self.userField.text forService:@"user" account:@"login"];
		            [SSKeychain setPassword:self.passField.text forService:@"pass" account:@"login"];
				}
		        [self showHud:NO];
		        [self performSegueWithIdentifier:@"LoginSegue" sender:nil];
			}
		    else {
		        NSString *errorString = [error userInfo][@"error"];
		        self.errorLabel.text = errorString;
			}
		    [self showHud:NO];
		}];
	}
	else {
		[self validationError];
	}
}

- (void)registration {
	if (self.userField.text.length && self.passField.text.length) {
		[self showHud:YES];
		PFUser *user = [PFUser user];
		user.username = self.userField.text.lowercaseString;
		user.password = [self hashed:self.passField.text.lowercaseString];
		[user signUpInBackgroundWithBlock: ^(BOOL succeeded, NSError *error) {
		    if (!error) {
		        if ([[NSUserDefaults standardUserDefaults] boolForKey:@"remember"]) {
		            [SSKeychain setPassword:self.userField.text forService:@"user" account:@"login"];
		            [SSKeychain setPassword:self.passField.text forService:@"pass" account:@"login"];
				}
		        [self showHud:NO];
		        [self performSegueWithIdentifier:@"LoginSegue" sender:nil];
			}
		    else {
		        NSString *errorString = [error userInfo][@"error"];
		        self.errorLabel.text = errorString;
			}
		    [self showHud:NO];
		}];
	}
	else {
		[self validationError];
	}
}

- (void)remChanged:(UISwitch *)swch {
	if (swch.isOn) {
		[[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"remember"];
	}
	else {
		[[NSUserDefaults standardUserDefaults] setBool:NO forKey:@"remember"];
		[SSKeychain deletePasswordForService:@"pass" account:@"main"];
		[SSKeychain deletePasswordForService:@"user" account:@"main"];
	}
	[[NSUserDefaults standardUserDefaults] synchronize];
}

- (void)validationError {
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Incomplete Fields" message:@"Please enter a username and password." delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
	[alert show];
}

- (void)showHud:(BOOL)show {
	if (!hud) {
		hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
		hud.labelText = @"Logging in...";
	}
	show ? [hud show:YES] : [hud hide:YES];
}

- (NSString *)hashed:(NSString *)input {
	const char *cstr = [input cStringUsingEncoding:NSUTF8StringEncoding];
	NSData *data = [NSData dataWithBytes:cstr length:input.length];
	uint8_t digest[CC_SHA256_DIGEST_LENGTH];
	CC_SHA256(data.bytes, (int)data.length, digest);
	NSMutableString *output = [NSMutableString stringWithCapacity:CC_SHA256_DIGEST_LENGTH * 2];
	for (int i = 0; i < CC_SHA256_DIGEST_LENGTH; i++) {
		[output appendFormat:@"%02x", digest[i]];
	}
	return output;
}

@end
