//
//  ChatTableViewController.h
//  msgs
//
//  Created by Paul on 24/06/2014.
//  Copyright (c) 2014 Paul. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MobileCoreServices/MobileCoreServices.h>
#import "CTAssetsPickerController.h"
#import "EXPhotoViewer.h"

@interface ChatTableViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, UITextViewDelegate, UIAlertViewDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate, CTAssetsPickerControllerDelegate, UIActionSheetDelegate> {
	int keyboardHeight;
	MBProgressHUD *hud;
	NSMutableArray *assetArray;
	UIImageView *zoomView;
	NSMutableArray *userArray;
}
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSArray *chats;
@property (strong, nonatomic) NSMutableArray *messages;
@property (strong, nonatomic) IBOutlet UIView *typeView;
@property (strong, nonatomic) IBOutlet UIButton *sendButton;
@property (strong, nonatomic) IBOutlet UITextView *textView;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *logoutButton;
@property (strong, nonatomic) UIRefreshControl *refreshControl;
@property (strong, nonatomic) IBOutlet UIButton *imageButton;
@property (strong, nonatomic) IBOutlet UIBarButtonItem *profPicButton;

@end
