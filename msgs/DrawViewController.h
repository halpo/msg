//
//  DrawViewController.h
//  msgs
//
//  Created by Paul on 28/06/2014.
//  Copyright (c) 2014 Paul. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "MyLineDrawingView.h"

@interface DrawViewController : UIViewController {
	MyLineDrawingView *drawView;
}

@property (strong, nonatomic) IBOutlet UIButton *undo;
@property (strong, nonatomic) IBOutlet UIButton *redo;
@property (strong, nonatomic) IBOutlet UIButton *send;
@property (strong, nonatomic) IBOutlet UIView *buttonBar;

@end
