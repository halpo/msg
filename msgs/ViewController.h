//
//  ViewController.h
//  msgs
//
//  Created by Paul on 24/06/2014.
//  Copyright (c) 2014 Paul. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CommonCrypto/CommonDigest.h>
#import "SSKeychain/SSKeychain.h"

@interface ViewController : UIViewController {
	MBProgressHUD *hud;
}
@property (strong, nonatomic) IBOutlet UITextField *userField;
@property (strong, nonatomic) IBOutlet UITextField *passField;
@property (strong, nonatomic) IBOutlet UIButton *loginButton;
@property (strong, nonatomic) IBOutlet UIButton *registerButton;
@property (strong, nonatomic) IBOutlet UILabel *errorLabel;
@property (strong, nonatomic) IBOutlet UISwitch *rememberSwitch;
@property (strong, nonatomic) IBOutlet UIButton *randomNameButton;
- (IBAction)bgTouch;

@end
