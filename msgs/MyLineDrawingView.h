//
//  MyLineDrawingView.h
//  msgs
//
//  Created by Paul on 24/06/2014.
//  Copyright (c) 2014 Paul. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface MyLineDrawingView : UIView {
	NSMutableArray *pathArray;
	NSMutableArray *bufferArray;
	UIBezierPath *myPath;
}
@property (nonatomic, assign) NSInteger undoSteps;
- (void)undoButtonClicked;
- (void)redoButtonClicked;
@end
