//
//  DrawViewController.m
//  msgs
//
//  Created by Paul on 28/06/2014.
//  Copyright (c) 2014 Paul. All rights reserved.
//

#import "DrawViewController.h"

@interface DrawViewController ()

@end

@implementation DrawViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if (self) {
		// Custom initialization
	}
	return self;
}

- (void)viewDidLoad {
	[super viewDidLoad];
	// Do any additional setup after loading the view.
	[self.view setBackgroundColor:[UIColor whiteColor]];
	drawView = [[MyLineDrawingView alloc]initWithFrame:CGRectMake(0, 44, 320, 568)];
	[drawView setBackgroundColor:[UIColor whiteColor]];
	[self.view addSubview:drawView];
	[self.view bringSubviewToFront:self.buttonBar];

	[self.undo addTarget:self action:@selector(undoPressed) forControlEvents:UIControlEventTouchUpInside];
	[self.redo addTarget:self action:@selector(redoPressed) forControlEvents:UIControlEventTouchUpInside];
	[self.send addTarget:self action:@selector(sendPressed) forControlEvents:UIControlEventTouchUpInside];
}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
	// Dispose of any resources that can be recreated.
}

#pragma mark buttons
- (void)undoPressed {
	[drawView undoButtonClicked];
}

- (void)redoPressed {
	[drawView redoButtonClicked];
}

- (void)sendPressed {
	self.buttonBar.hidden = YES;
	[self performSelector:@selector(getDrawing) withObject:nil afterDelay:0.05];
}

- (void)getDrawing {
	UIGraphicsBeginImageContext(self.view.frame.size);
	[self.view.layer renderInContext:UIGraphicsGetCurrentContext()];
	UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();

	//Crop the image
	CGImageRef imageRef = CGImageCreateWithImageInRect([img CGImage], CGRectMake(0, 44, 320, 568));
	img = [UIImage imageWithCGImage:imageRef];
	self.buttonBar.hidden = NO;

	//Send message
	NSData *imageData = UIImageJPEGRepresentation(img, 0.5);
	PFFile *imageFile = [PFFile fileWithName:@"img.jpg" data:imageData];
	PFObject *newMessage = [PFObject objectWithClassName:@"message"];
	[newMessage setObject:[PFUser currentUser] forKey:@"user"];
	[newMessage setObject:imageFile forKey:@"image"];
	[newMessage save];
	[self.navigationController popViewControllerAnimated:YES];
}

@end
