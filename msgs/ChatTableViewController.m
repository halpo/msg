//
//  ChatTableViewController.m
//  msgs
//
//  Created by Paul on 24/06/2014.
//  Copyright (c) 2014 Paul. All rights reserved.
//

#import "ChatTableViewController.h"

@interface ChatTableViewController ()

@end

@implementation ChatTableViewController

#pragma mark setup

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil {
	self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
	if (self) {
	}
	return self;
}

- (void)viewDidLoad {
	[super viewDidLoad];
	self.title = [NSString stringWithFormat:@"%@", [[PFUser currentUser].username capitalizedString]];
	if (!hud) {
		hud = [MBProgressHUD showHUDAddedTo:self.view animated:YES];
		hud.labelText = @"Loading...";
	}
	self.typeView.backgroundColor = [UIColor lightGrayColor];
	[self.sendButton addTarget:self action:@selector(sendMessage:) forControlEvents:UIControlEventTouchUpInside];
	self.logoutButton.target = self;
	self.logoutButton.action = @selector(logout);
	[self.imageButton addTarget:self action:@selector(sendImage) forControlEvents:UIControlEventTouchUpInside];
	self.profPicButton.target = self;
	self.profPicButton.action = @selector(checkUserPhoto);

	if (!self.refreshControl) {
		self.refreshControl = [[UIRefreshControl alloc] init];
		self.refreshControl.tintColor = [UIColor orangeColor];
		[self.refreshControl addTarget:self action:@selector(viewDidAppear:) forControlEvents:UIControlEventValueChanged];
		UITableViewController *tvc = [[UITableViewController alloc] initWithStyle:self.tableView.style];
		tvc.tableView = self.tableView;
		tvc.refreshControl = self.refreshControl;
		[self addChildViewController:tvc];
	}

	if (iOS8) {
		keyboardHeight = kKeyboardHeight + 38;
	}
	else {
		keyboardHeight = kKeyboardHeight;
	}
}

- (void)viewDidAppear:(BOOL)animated {
	PFQuery *query = [PFQuery queryWithClassName:@"message"];
	[query includeKey:@"user"];
	[query orderByDescending:@"createdAt"];
	query.limit = 25;
	[query findObjectsInBackgroundWithBlock: ^(NSArray *objects, NSError *error) {
	    if (!error) {
	        self.chats = objects;
	        [self.tableView reloadData];
		}
	    else {
	        
		}
        
        self.messages = [NSMutableArray new];
        
        
        for (PFObject *obj in self.chats) {
            PFUser *user = [obj objectForKey:@"user"];
            PFFile *profPicFile = [user objectForKey:@"profilePic"];
            UIImage *profPicture;
            if (profPicFile) {
                NSData *profImageData = [profPicFile getData];
                profPicture = [UIImage imageWithData:profImageData];
            }
            
            PFFile *imageFile = [obj objectForKey:@"image"];
            UIImage *picture;
            if (imageFile) {
                NSData *imageData = [imageFile getData];
                picture = [UIImage imageWithData:imageData];
                
            }
            
            NSDictionary *messageDict = @{@"username": [user username],
                            @"isPicture":[NSNumber numberWithBool:imageFile?YES:NO],
                            @"picture":picture?:@"",
                            @"message":[obj valueForKey:@"messageText"]?:[NSNull null],
                            @"profPic":profPicture?:[NSNull null]
                            };
            
            
            [self.messages addObject:messageDict];
        }
        
	    [hud hide:YES];
	    [self.refreshControl endRefreshing];
	}];
}

- (void)didReceiveMemoryWarning {
	[super didReceiveMemoryWarning];
}

- (void)checkUserPhoto {
	UIAlertView *newProfAlert = [[UIAlertView alloc] initWithTitle:@"Profile Pic" message:@"Take / Change prof pic??????" delegate:self cancelButtonTitle:@"nah" otherButtonTitles:@"fair!", nil];
	[newProfAlert show];
}

#pragma mark buttons

- (void)sendMessage:(UIButton *)btn {
	[self.view endEditing:YES];
	if (self.textView.text.length) {
		[hud show:YES];
		PFObject *newMessage = [PFObject objectWithClassName:@"message"];
		[newMessage setObject:self.textView.text forKey:@"messageText"];
		[newMessage setObject:[PFUser currentUser] forKey:@"user"];
		[newMessage save];
		self.textView.text = @"";
		[self viewDidAppear:YES];
	}
}

- (void)sendImage {
	UIActionSheet *imageSourceChoose = [[UIActionSheet alloc] initWithTitle:nil delegate:self cancelButtonTitle:@"Cancel" destructiveButtonTitle:nil otherButtonTitles:@"Library", @"Camera", @"Draw", nil];
	[imageSourceChoose showInView:self.view];
}

- (void)logout {
	[PFUser logOut];
	[self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark tableView
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
	return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
	return self.chats.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
	PFObject *messageObject = [self.chats objectAtIndex:indexPath.row];
	CGFloat height = 80;
	if ([messageObject objectForKey:@"image"] != nil) {
		height = 322;
	}
	return height;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {

	UITableViewCell *cell = nil;
    
    NSDictionary *messageDict = [self.messages objectAtIndex:indexPath.row];
    

	if ([[messageDict valueForKey:@"isPicture"] boolValue]) {
		cell = [tableView dequeueReusableCellWithIdentifier:@"ImageCell"];
		if (!cell) {
			cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"ImageCell"];
		}
		UIImageView *imageView = (UIImageView *)[cell viewWithTag:6];
		UIImageView *profilePicView = (UIImageView *)[cell viewWithTag:4];
		UILabel *userLabel = (UILabel *)[cell viewWithTag:5];
        
		UIImage *picture = (UIImage *)[messageDict objectForKey:@"picture"];
		[imageView setContentMode:UIViewContentModeScaleAspectFit];
		[imageView setImage:picture];

		UIImage *profPic = [messageDict objectForKey:@"profPic"];
		if ([profPic class] == [UIImage class]) {
			[profilePicView setImage:profPic];
		}
		else {
			[profilePicView setImage:nil];
		}

		userLabel.text = [[messageDict valueForKey:@"username"] capitalizedString];
	}
	else {
		cell = [tableView dequeueReusableCellWithIdentifier:@"ChatTableCell"];
		if (!cell) {
			cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"ChatTableCell"];
		}
		UILabel *userLabel = (UILabel *)[cell viewWithTag:2];
		UILabel *messageLabel = (UILabel *)[cell viewWithTag:3];
		UIImageView *profilePicView = (UIImageView *)[cell viewWithTag:1];

        UIImage *picture = [messageDict objectForKey:@"profPic"];
        if ([picture class] == [UIImage class]) {
            [profilePicView setImage:picture];
		}
		else {
			[profilePicView setImage:nil];
		}

		userLabel.text = [[messageDict valueForKey:@"username"] capitalizedString];
		messageLabel.text = [messageDict valueForKey:@"message"];
	}

	cell.clipsToBounds = YES;
	return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
	[tableView deselectRowAtIndexPath:indexPath animated:YES];
	PFObject *messageObject = [self.chats objectAtIndex:indexPath.row];
	PFUser *user = [messageObject objectForKey:@"user"];

	if ([messageObject objectForKey:@"image"] != nil) {
		if (!zoomView) {
			zoomView = [UIImageView new];
		}
		PFFile *imageFile = [messageObject objectForKey:@"image"];
		NSData *imageData = [imageFile getData];
		UIImage *picture = [UIImage imageWithData:imageData];
		[zoomView setImage:picture];
		[EXPhotoViewer showImageFrom:zoomView];
	}
	else {
		UIAlertView *fullMessage = [[UIAlertView alloc] initWithTitle:[user valueForKey:@"username"] message:[messageObject valueForKey:@"messageText"] delegate:nil cancelButtonTitle:@"Nice one!" otherButtonTitles:nil];
		[fullMessage show];
	}
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath {
	PFObject *messageObject = [self.chats objectAtIndex:indexPath.row];
	PFUser *user = [messageObject objectForKey:@"user"];
	if ([user.username isEqualToString:[PFUser currentUser].username]) {
		return YES;
	}
	else {
		return NO;
	}
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
	PFObject *messageObject = [self.chats objectAtIndex:indexPath.row];
	PFUser *user = [messageObject objectForKey:@"user"];
	if ([user.username isEqualToString:[PFUser currentUser].username]) {
		[messageObject deleteInBackgroundWithBlock: ^(BOOL succeeded, NSError *error) {
		    if (!error) {
		        [self viewDidAppear:YES];
			}
		}];
	}
}

#pragma mark textView
- (void)textViewDidBeginEditing:(UITextView *)textView {
	[UIView animateWithDuration:0.3 animations: ^{
	    self.typeView.frame = CGRectOffset(self.typeView.frame, 0, -keyboardHeight);
	}];
}

- (void)textViewDidEndEditing:(UITextView *)textView {
	[UIView animateWithDuration:0.3 animations: ^{
	    self.typeView.frame = CGRectOffset(self.typeView.frame, 0, keyboardHeight);
	}];
}

#pragma mark alertView
- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex {
	switch (buttonIndex) {
		case 1: {
			if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
				[self cameraImageForMessage:NO];
			}
			else {
				UIAlertView *noCameraAlert = [[UIAlertView alloc] initWithTitle:@"No camera on this device" message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
				[noCameraAlert show];
			}
			break;
		}

		default:
			break;
	}
}

#pragma mark actionSheet
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex {
	switch (buttonIndex) {
		case 0: {
			if (iOS6) {
				UIAlertView *ios6Alert = [[UIAlertView alloc] initWithTitle:@"Not supported on this OS" message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
				[ios6Alert show];
			}
			else {
				[self libraryImage];
			}
			break;
		}

		case 1: {
			if ([UIImagePickerController isSourceTypeAvailable:UIImagePickerControllerSourceTypeCamera]) {
				[self cameraImageForMessage:YES];
			}
			else {
				UIAlertView *noCameraAlert = [[UIAlertView alloc] initWithTitle:@"No camera on this device" message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
				[noCameraAlert show];
			}
			break;
		}

		case 2:
			[self performSegueWithIdentifier:@"DrawSegue" sender:nil];
			break;

		default:
			break;
	}
}

#pragma mark imageStuff
- (UIImage *)resizeImage:(UIImage *)image width:(CGFloat)resizedWidth height:(CGFloat)resizedHeight {
	CGImageRef imageRef = [image CGImage];
	CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
	CGContextRef bitmap = CGBitmapContextCreate(NULL, resizedWidth, resizedHeight, 8, 4 * resizedWidth, colorSpace, (CGBitmapInfo)kCGImageAlphaPremultipliedFirst);

	CGContextDrawImage(bitmap, CGRectMake(0, 0, resizedWidth, resizedHeight), imageRef);
	CGImageRef ref = CGBitmapContextCreateImage(bitmap);
	UIImage *result;

	result = [UIImage imageWithCGImage:ref];

	CGImageRelease(ref);
	CGContextRelease(bitmap);
	return result;
}

- (UIImage *)normalizedImage:(UIImage *)input {
	if (input.imageOrientation == UIImageOrientationUp) {
		return input;
	}
	UIGraphicsBeginImageContextWithOptions(input.size, NO, input.scale);
	[input drawInRect:(CGRect) {0, 0, input.size }];
	UIImage *normalizedImage = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	return normalizedImage;
}

#pragma mark image Picker
- (void)libraryImage {
	CTAssetsPickerController *picker = [[CTAssetsPickerController alloc] init];
	picker.delegate = self;
	picker.assetsFilter = [ALAssetsFilter allPhotos];
	picker.selectedAssets = [NSMutableArray arrayWithArray:assetArray];
	[self presentViewController:picker animated:YES completion:nil];
}

- (BOOL)assetsPickerController:(CTAssetsPickerController *)picker shouldSelectAsset:(ALAsset *)asset {
	if (picker.selectedAssets.count >= 1) {
		return NO;
	}
	else {
		return YES;
	}
}

- (void)assetsPickerController:(CTAssetsPickerController *)picker didFinishPickingAssets:(NSArray *)assets {
	[picker.presentingViewController dismissViewControllerAnimated:YES completion:nil];
	[hud show:YES];
	[self.view endEditing:YES];
	ALAssetRepresentation *defaultRep = [assets[0] defaultRepresentation];
	UIImage *img = [UIImage imageWithCGImage:[defaultRep fullResolutionImage] scale:[defaultRep scale] orientation:(UIImageOrientation)[defaultRep orientation]];
	img = [self normalizedImage:img];
	NSData *imageData = UIImageJPEGRepresentation(img, 0.1);
	PFFile *imageFile = [PFFile fileWithName:@"img.jpg" data:imageData];
	PFObject *newMessage = [PFObject objectWithClassName:@"message"];
	[newMessage setObject:[PFUser currentUser] forKey:@"user"];
	[newMessage setObject:imageFile forKey:@"image"];
	[newMessage save];
	self.textView.text = @"";
	[self viewDidAppear:YES];
}

- (void)cameraImageForMessage:(BOOL)message {
	UIImagePickerController *imagePicker = [[UIImagePickerController alloc] init];
	imagePicker.delegate = self;
	imagePicker.sourceType = UIImagePickerControllerSourceTypeCamera;
	imagePicker.mediaTypes = @[(NSString *)kUTTypeImage];
	if (message) {
		imagePicker.view.tag = 1;
	}
	else {
		imagePicker.view.tag = 2;
	}
	[self presentViewController:imagePicker animated:YES completion:nil];
}

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
	UIImage *image = [info objectForKey:UIImagePickerControllerOriginalImage];
	[self dismissViewControllerAnimated:YES completion:nil];
	if (image) {
		image = [self normalizedImage:image];
		if (picker.view.tag == 2) {
			image = [self resizeImage:image width:200 height:200];
			NSData *imgData = UIImageJPEGRepresentation(image, 0.1);
			PFFile *imageFile = [PFFile fileWithName:@"img.jpg" data:imgData];
			PFUser *currentUser = [PFUser currentUser];
			[currentUser setObject:imageFile forKey:@"profilePic"];
			[currentUser save];
		}
		else if (picker.view.tag == 1) {
			[hud show:YES];
			[self.view endEditing:YES];
			NSData *imgData = UIImageJPEGRepresentation(image, 0.1);
			PFFile *imageFile = [PFFile fileWithName:@"img.jpg" data:imgData];
			PFObject *newMessage = [PFObject objectWithClassName:@"message"];
			[newMessage setObject:[PFUser currentUser] forKey:@"user"];
			[newMessage setObject:imageFile forKey:@"image"];
			[newMessage save];
			self.textView.text = @"";
			[self viewDidAppear:YES];
		}
	}
}

@end
