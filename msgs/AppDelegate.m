//
//  AppDelegate.m
//  msgs
//
//  Created by Paul on 24/06/2014.
//  Copyright (c) 2014 Paul. All rights reserved.
//

#import "AppDelegate.h"

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
	[Parse setApplicationId:@"MmJ4MTTle9dkM8mitQf9C5cqW1Kl0ufBHWLDhcn6" clientKey:@"fuUToJQIM167jOzWXo27lu6KaN20jLETGrUyvHCF"];
	[PFAnalytics trackAppOpenedWithLaunchOptions:launchOptions];
	[application registerForRemoteNotificationTypes:UIRemoteNotificationTypeBadge | UIRemoteNotificationTypeAlert | UIRemoteNotificationTypeSound];
	return YES;
}

- (void)application:(UIApplication *)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData *)deviceToken {
	PFInstallation *currentInstallation = [PFInstallation currentInstallation];
	[currentInstallation setDeviceTokenFromData:deviceToken];
	[currentInstallation saveInBackground];
}

- (void)application:(UIApplication *)application didReceiveRemoteNotification:(NSDictionary *)userInfo {
}

- (void)applicationWillResignActive:(UIApplication *)application {
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
	application.applicationIconBadgeNumber = 0;
}

- (void)applicationWillTerminate:(UIApplication *)application {
	application.applicationIconBadgeNumber = 0;
	if (![[NSUserDefaults standardUserDefaults] boolForKey:@"remember"]) {
		[PFUser logOut];
	}
}

@end
